<?php
session_start();

if (!isset($_SESSION["username"])) {
    header("Location: login.php");
    exit;
}

$mysqli = require __DIR__ . "/database.php";

$sqlClients = "SELECT clients.last_name, clients.name, users.userID, users.username, users.email, clients.address, clients.phone 
               FROM clients 
               JOIN users ON clients.userID = users.userID";
$resultClients = $mysqli->query($sqlClients);

$sqlDentists = "SELECT dentists.last_name, dentists.name, users.userID, users.username, users.email 
                FROM dentists 
                JOIN users ON dentists.userID = users.userID";
$resultDentists = $mysqli->query($sqlDentists);

$sqlReceptionists = "SELECT receptionists.last_name, receptionists.name, users.userID, users.username, users.email 
                     FROM receptionists 
                     JOIN users ON receptionists.userID = users.userID";
$resultReceptionists = $mysqli->query($sqlReceptionists);

$deleteSuccess = false;

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['delete_user'])) {
    $userID = $_POST['delete_user'];
    
    $stmt = $mysqli->prepare("DELETE FROM clients WHERE userID = ?");
    $stmt->bind_param("i", $userID);
    $stmt->execute();
    
    $stmt = $mysqli->prepare("DELETE FROM dentists WHERE userID = ?");
    $stmt->bind_param("i", $userID);
    $stmt->execute();
    
    $stmt = $mysqli->prepare("DELETE FROM receptionists WHERE userID = ?");
    $stmt->bind_param("i", $userID);
    $stmt->execute();

    $stmt = $mysqli->prepare("DELETE FROM users WHERE userID = ?");
    $stmt->bind_param("i", $userID);
    $stmt->execute();

    $deleteSuccess = true;
}
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrare Utilizatori</title>
    <link rel="stylesheet" href="receptionisthomestyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
    <style>
        .confirm-delete {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: white;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            z-index: 1001;
        }

        .confirm-delete p {
            font-size: 18px;
            margin-bottom: 20px;
        }

        .confirm-delete button {
            margin: 10px;
            padding: 10px 20px;
            font-size: 16px;
            cursor: pointer;
            border: none;
            border-radius: 5px;
            transition: background-color 0.3s;
        }

        .confirm-delete button:hover {
            background-color: #ddd;
        }

        .overlay {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            z-index: 1000;
        }
    </style>
</head>
<body>
    <div class="overlay" id="overlay"></div>
    <div class="confirm-logout" id="confirmLogout">
        <p>Doriți să ieșiți din cont?</p>
        <button onclick="confirmLogout(true)">Da</button>
        <button onclick="confirmLogout(false)">Nu</button>
    </div>

    <div class="navbar">
        <a href="receptionisthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="receptionistprofil.php">Profilul meu</a>
                <a href="administrareutilizatori.php">Administrare utilizatori</a>
                <a href="receptionistappointments.php">Administrare programări</a>
            </div>
        </div>
        <a href="#" class="nav-item" id="logout">Delogare</a>
    </div>
    <div class="content">
        <h1>Administrare Utilizatori</h1>
        <div class="main-container">
            <table>
                <h2>Clienti:</h2>
                <thead>
                    <tr>
                        <th>Nume</th>
                        <th>Prenume</th>
                        <th>Nume de utilizator</th>
                        <th>Email</th>
                        <th>Adresa</th>
                        <th>Telefon</th>
                        <th>Acțiuni</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($client = $resultClients->fetch_assoc()): ?>
                        <tr>
                            <td><?= htmlspecialchars($client['last_name']) ?></td>
                            <td><?= htmlspecialchars($client['name']) ?></td>
                            <td><?= htmlspecialchars($client['username']) ?></td>
                            <td><?= htmlspecialchars($client['email']) ?></td>
                            <td><?= htmlspecialchars($client['address']) ?></td>
                            <td><?= htmlspecialchars($client['phone']) ?></td>
                            <td>
                                <button class="delete-button" onclick="confirmDelete(<?= $client['userID'] ?>)">șterge</button>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
            <button class="add-client-button" onclick="location.href='adauga_client.php'">Adaugă un client nou</button>
        </div>
        
        <div class="main-container">
            <table>
            <h2>Medici stomatologi:</h2>
                <thead>
                    <tr>
                        <th>Nume</th>
                        <th>Prenume</th>
                        <th>Nume de utilizator</th>
                        <th>Email</th>
                        <th>Acțiuni</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($dentist = $resultDentists->fetch_assoc()): ?>
                        <tr>
                            <td><?= htmlspecialchars($dentist['last_name']) ?></td>
                            <td><?= htmlspecialchars($dentist['name']) ?></td>
                            <td><?= htmlspecialchars($dentist['username']) ?></td>
                            <td><?= htmlspecialchars($dentist['email']) ?></td>
                            <td>
                                <button class="delete-button" onclick="confirmDelete(<?= $dentist['userID'] ?>)">sterge</button>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
            <button class="add-client-button" onclick="location.href='adauga_dentist.php'">Adaugă un medic stomatolog nou</button>
        </div>
        <div class="main-container">
            <table>
            <h2>Recepționiști:</h2>
                <thead>
                    <tr>
                        <th>Nume</th>
                        <th>Prenume</th>
                        <th>Nume de utilizator</th>
                        <th>Email</th>
                        <th>Acțiuni</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($receptionist = $resultReceptionists->fetch_assoc()): ?>
                        <tr>
                            <td><?= htmlspecialchars($receptionist['last_name']) ?></td>
                            <td><?= htmlspecialchars($receptionist['name']) ?></td>
                            <td><?= htmlspecialchars($receptionist['username']) ?></td>
                            <td><?= htmlspecialchars($receptionist['email']) ?></td>
                            <td>
                                <button class="delete-button" onclick="confirmDelete(<?= $receptionist['userID'] ?>)">sterge</button>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
            <button class="add-client-button" onclick="location.href='adauga_receptionist.php'">Adaugă un recepționist nou</button>
        </div>
    </div>

    <div class="overlay" id="deleteOverlay"></div>
    <div class="confirm-delete" id="confirmDeleteDialog">
        <p>Doriți să ștergeți acest utilizator?</p>
        <form method="post" action="administrareutilizatori.php">
            <input type="hidden" name="delete_user" id="deleteUserID">
            <button type="submit">Da</button>
            <button type="button" onclick="closeConfirmDelete()">Nu</button>
        </form>
    </div>

    <?php if ($deleteSuccess): ?>
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Utilizatorul a fost șters',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                window.location.href = 'administrareutilizatori.php'; 
            });
        </script>
    <?php endif; ?>

    <script>
        function confirmDelete(userID) {
            document.getElementById('deleteUserID').value = userID;
            document.getElementById('deleteOverlay').style.display = 'block';
            document.getElementById('confirmDeleteDialog').style.display = 'block';
        }

        function closeConfirmDelete() {
            document.getElementById('deleteOverlay').style.display = 'none';
            document.getElementById('confirmDeleteDialog').style.display = 'none';
        }

        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('confirmLogout').style.display = 'block';
            document.getElementById('overlay').style.display = 'block';
        });

        function confirmLogout(choice) {
            if (choice) {
                window.location.href = 'login.php';
            } else {
                document.getElementById('confirmLogout').style.display = 'none';
                document.getElementById('overlay').style.display = 'none';
            }
        }
    </script>
</body>
</html>
