<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$mysqli = require __DIR__ . "/database.php";

$errors = [];

if (empty($_POST["last_name"])) {
    $errors['last_name'] = "Câmpul este gol";
}

if (empty($_POST["name"])) {
    $errors['name'] = "Câmpul este gol";
}

if (empty($_POST["username"])) {
    $errors['username'] = "Câmpul este gol";
} else if (strlen($_POST["username"]) < 5) {
    $errors['username'] = "Numele de utilizator trebuie să conțină cel puțin 5 caractere";
} else {
    $sql = "SELECT COUNT(*) FROM users WHERE username = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("s", $_POST["username"]);
    $stmt->execute();
    $stmt->bind_result($username_count);
    $stmt->fetch();
    if ($username_count > 0) {
        $errors['username'] = "Numele de utilizator deja ocupat";
    }
    $stmt->close();
}

if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
    $errors['email'] = "Introduceți un email valid";
} else {
    $sql = "SELECT COUNT(*) FROM users WHERE email = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("s", $_POST["email"]);
    $stmt->execute();
    $stmt->bind_result($email_count);
    $stmt->fetch();
    if ($email_count > 0) {
        $errors['email'] = "Email-ul deja ocupat";
    }
    $stmt->close();
}

if (empty($_POST["adress"])) {
    $errors['adress'] = "Câmpul este gol";
}

if (strlen($_POST["phone"]) < 7) {
    $errors['phone'] = "Număr de telefon invalid";
}

if (strlen($_POST["password"]) < 8) {
    $errors['password'] = "Parola trebuie să conțină cel puțin 8 caractere";
}

if ($_POST["password"] !== $_POST["password_confirmation"]) {
    $errors['password_confirmation'] = "Parola nu se potrivește";
}

if (!empty($errors)) {
    header('Content-Type: application/json');
    echo json_encode(['errors' => $errors]);
    exit;
}

$password_hash = password_hash($_POST["password"], PASSWORD_DEFAULT);

$sql = "INSERT INTO users (username, email, password_hash, role) VALUES (?, ?, ?, 'client')";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param("sss", $_POST["username"], $_POST["email"], $password_hash);

if ($stmt->execute()) {
    $user_id = $stmt->insert_id;
    
    $sql = "INSERT INTO clients (userID, last_name, name, address, phone) VALUES (?, ?, ?, ?, ?)";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("issss", $user_id, $_POST["last_name"], $_POST["name"], $_POST["adress"], $_POST["phone"]);
    
    if ($stmt->execute()) {
        echo json_encode(['success' => true]);
        exit; 
    } else {
        $errors['database'] = "Error inserting into clients table: " . $stmt->error;
        header('Content-Type: application/json');
        echo json_encode(['errors' => $errors]);
        exit; 
    }
} else {
    if ($mysqli->errno === 1062) {
        $errors['username'] = "Numele de utilizator deja ocupat";
        $errors['email'] = "Email-ul deja ocupat";
        header('Content-Type: application/json');
        echo json_encode(['errors' => $errors]);
    } else {
        $errors['database'] = "Error inserting into users table: " . $stmt->error;
        header('Content-Type: application/json');
        echo json_encode(['errors' => $errors]);
    }
    exit; 
}

?>
