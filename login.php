<?php
session_start();

$is_invalid = false;

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $mysqli = require __DIR__ . "/database.php";

    $sql = sprintf("SELECT * FROM users WHERE email='%s'", $mysqli->real_escape_string($_POST["email"]));
    $result = $mysqli->query($sql);
    $user = $result->fetch_assoc();

    if ($user) {
        if (password_verify($_POST["password"], $user["password_hash"])) {
            $_SESSION["user_id"] = $user["userID"];
            $_SESSION["username"] = $user["username"];
            $_SESSION["role"] = $user["role"];
            switch ($user["role"]) {
                case 'client':
                    header("Location: clienthome.php");
                    break;
                case 'dentist':
                    header("Location: dentisthome.php");
                    break;
                case 'receptionist':
                    header("Location: receptionisthome.php");
                    break;
                default:
                    header("Location: home.php");
                    break;
            }
            exit;
        }
    }

    $is_invalid = true;
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="loginstyle.css">
</head>
<body>
    <div class="login-container">
        <h1>Autentificare</h1>
        <form id="login-form" method="post" novalidate>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" placeholder="Introduceți email-ul" value="<?= htmlspecialchars($_POST["email"] ?? "") ?>">
                <?php if ($is_invalid): ?>
                    <div class="error-message">Parola sau email invalid</div>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <label for="password">Parola:</label>
                <input type="password" id="password" name="password" placeholder="Introduceți parola">
                <?php if ($is_invalid): ?>
                    <div class="error-message">Parola sau email invalid</div>
                <?php endif; ?>
            </div>
            <button type="submit">Log in</button>
            <div class="signup-link">
                Client nou? <a href="signup.html">Înregistrare</a>.
            </div>
        </form>
    </div>
</body>
</html>
