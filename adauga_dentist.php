<?php
$mysqli = require __DIR__ . "/database.php";

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $last_name = $_POST['last_name'];
    $name = $_POST['name'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

    $sqlUser = "INSERT INTO users (username, email, password_hash, role) VALUES (?, ?, ?, 'dentist')";
    $stmtUser = $mysqli->prepare($sqlUser);
    $stmtUser->bind_param("sss", $username, $email, $password);
    $stmtUser->execute();
    $userID = $stmtUser->insert_id;

    $sqlDentist = "INSERT INTO dentists (userID, last_name, name) VALUES (?, ?, ?)";
    $stmtDentist = $mysqli->prepare($sqlDentist);
    $stmtDentist->bind_param("iss", $userID, $last_name, $name);
    $stmtDentist->execute();

    echo "<script>
            document.addEventListener('DOMContentLoaded', function() {
                Swal.fire({
                    icon: 'success',
                    title: 'Adăugare reușită!',
                    showConfirmButton: false,
                    timer: 1500
                }).then(() => {
                    window.location.href = 'administrareutilizatori.php';
                });
            });
          </script>";
}
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Adauga Dentist</title>
    <link rel="stylesheet" href="receptionisthomestyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
</head>
<body>
    <div class="navbar">
        <a href="receptionisthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="receptionisthome.php">Profilul meu</a>
                <a href="administrareutilizatori.php">Administrare utilizatori</a>
                <a href="receptionistappointments.php">Administrare programări</a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="form-container">
            <form method="post" action="adauga_dentist.php">
                <h2>Adaugă Medic Stomatolog Nou</h2>
                <label for="last_name"><b>Nume:</b></label>
                <input type="text" placeholder="Enter Last Name" name="last_name" required>

                <label for="name"><b>Prenume:</b></label>
                <input type="text" placeholder="Enter First Name" name="name" required>

                <label for="username"><b>Nume de utilizator:</b></label>
                <input type="text" placeholder="Enter Username" name="username" required>

                <label for="email"><b>Email:</b></label>
                <input type="email" placeholder="Enter Email" name="email" required>

                <label for="password"><b>Parola:</b></label>
                <input type="password" placeholder="Enter Password" name="password" required>

                <button type="submit" class="btn">Adaugare</button>
                <button type="button" class="btn cancel" onclick="location.href='administrareutilizatori.php'">Înapoi</button>
            </form>
        </div>
    </div>
</body>
</html>
