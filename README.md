# Aplicație web pentru cabinetele stomatologice

Aceasta este o aplicație web pentru gestionarea programărilor, utilizatorilor și a altor sarcini administrative într-un cabinet stomatologic. Aplicația permite clienților, medicilor stomatologi și recepționiștilor să interacționeze cu sistemul prin diferite interfețe și funcționalități.

## Repository

Codul sursă este disponibil pe GitLab: [Link Repository GitLab](https://gitlab.upt.ro/stefan.mata/aplica-ie-web-pentru-cabinetele-stomatologice)

## Structura Proiectului

- `adauga_client.php`, `adauga_dentist.php`, `adauga_receptionist.php`: Scripturi pentru adăugarea utilizatorilor.
- `administrareutilizatori.php`: Administrarea utilizatorilor.
- `clientappointments.php`, `dentistappointments.php`, `receptionistappointments.php`: Gestionarea programărilor.
- `clienthome.php`, `dentisthome.php`, `receptionisthome.php`: Pagini de start pentru utilizatori.
- `clientprofil.php`, `dentistprofil.php`, `receptionistprofil.php`: Gestionarea profilului utilizatorilor.
- `common.js`, `validate.js`: Scripturi JavaScript pentru funcționalități comune și validare.
- `database.php`: Conexiunea la baza de date.
- `login.php`, `signup.html`: Pagini de autentificare și înregistrare.
- `generate_invoice.php`: Generarea facturilor.

## Tehnologii Utilizate

PHP, MySQL, JavaScript, HTML/CSS

## Pași de Configurare și Instalare

1. Clonează repository-ul:
   ```bash
git clone https://gitlab.upt.ro/stefan.mata/aplica-ie-web-pentru-cabinetele-stomatologice

2. Accesarea Directorului Proiectului: După clonare, accesați directorul proiectului folosind comanda:
    ```bash 
cd aplica-ie-web-pentru-cabinetele-stomatologice

3. Configurarea Bazei de Date MySQL: Asigurați-vă că aveți un server MySQL instalat și funcțional. Creați o bază de date nouă denumită dentist_office. Importați schema bazei de date (dacă există un fișier SQL inclus) folosind unelte precum phpMyAdmin sau linia de comandă MySQL.

4. Configurarea Conexiunii la Baza de Date: Deschideți fișierul database.php și modificați detaliile conexiunii (nume utilizator, parolă, nume bază de date) pentru a corespunde cu configurarea locală a MySQL.

## Rularea Aplicației

1. Plasează fișierele în directorul serverului web: Copiați toate fișierele proiectului în directorul rădăcină al serverului web (ex. htdocs pentru XAMPP).

2. Pornește serverul web: Lansați serverul web (Apache) folosind control panel-ul XAMPP sau un alt server web local.

3. Accesează aplicația: Deschideți un browser web și navigați la http://localhost/{numele-folderului-proiectului} pentru a ac

## Utilizare 

1. Autentificare sau Înregistrare: Accesați login.php pentru a vă autentifica dacă aveți deja un cont sau signup.html pentru a crea un cont 

2. Gestionarea Programărilor și Profilurilor: Odată autentificat, utilizați interfața pentru a adăuga, vizualiza sau modifica programări, precum și pentru a gestiona informațiile de profil.






   git clone https://gitlab.upt.ro/stefan.mata/aplica-ie-web-pentru-cabinetele-stomatologice
