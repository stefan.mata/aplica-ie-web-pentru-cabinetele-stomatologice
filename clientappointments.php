<?php
session_start();

if (!isset($_SESSION["user_id"])) {
    header("Location: login.php");
    exit;
}

$mysqli = require __DIR__ . "/database.php";

$user_id = $_SESSION["user_id"];
$sqlClientID = "SELECT clientID FROM clients WHERE userID = ?";
$stmtClientID = $mysqli->prepare($sqlClientID);
$stmtClientID->bind_param("i", $user_id);
$stmtClientID->execute();
$resultClientID = $stmtClientID->get_result();
$client = $resultClientID->fetch_assoc();
$clientID = $client['clientID'];

$sql = "SELECT appointments.appointmentID, appointments.type, dentists.last_name, dentists.name, appointments.date, appointments.time, appointments.payment
        FROM appointments
        JOIN dentists ON appointments.dentistID = dentists.dentistID
        WHERE appointments.clientID = ? AND appointments.confirm = 'Yes'";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param("i", $clientID);
$stmt->execute();
$result = $stmt->get_result();

$sqlDentists = "SELECT dentistID, last_name, name FROM dentists";
$dentistsResult = $mysqli->query($sqlDentists);

$sqlTypes = "SHOW COLUMNS FROM appointments LIKE 'type'";
$typesResult = $mysqli->query($sqlTypes);
$typeRow = $typesResult->fetch_assoc();
preg_match("/^enum\(\'(.*)\'\)$/", $typeRow['Type'], $matches);
$types = explode("','", $matches[1]);

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['type']) && isset($_POST['dentistID']) && isset($_POST['date']) && isset($_POST['time'])) {
    $type = $_POST['type'];
    $dentistID = $_POST['dentistID'];
    $date = $_POST['date'];
    $time = $_POST['time'];

    $sqlInsert = "INSERT INTO appointments (clientID, dentistID, type, date, time, confirm, payment) VALUES (?, ?, ?, ?, ?, 'No', NULL)";
    $stmtInsert = $mysqli->prepare($sqlInsert);
    $stmtInsert->bind_param("iisss", $clientID, $dentistID, $type, $date, $time);
    $stmtInsert->execute();

    header("Location: clientappointments.php?appointment_success=1");
    exit;
}

if (isset($_POST['cancel_appointment'])) {
    $appointmentID = $_POST['cancel_appointment'];

    $sqlDelete = "DELETE FROM appointments WHERE appointmentID = ?";
    $stmtDelete = $mysqli->prepare($sqlDelete);
    $stmtDelete->bind_param("i", $appointmentID);
    $stmtDelete->execute();

    header("Location: clientappointments.php?cancel_success=1");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Programările Mele</title>
    <link rel="stylesheet" href="clientappointmentsstyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
    <div class="navbar">
        <a href="clienthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="clientprofil.php">Profilul meu</a>
                <a href="clientappointments.php">Programările mele</a>
            </div>
        </div>
        <a href="login.php" class="nav-item" id="logout">Delogare</a>
    </div>
    <div class="content">
        <h1>Programările Mele</h1>
        <div class="table-container">
            <table>
                <thead>
                    <tr>
                        <th>Tip</th>
                        <th>Medic stomatolog</th>
                        <th>Data</th>
                        <th>Ora</th>
                        <th>Plată</th>
                        <th>Acțiuni</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($appointment = $result->fetch_assoc()): ?>
                        <tr>
                            <td><?= htmlspecialchars($appointment['type']) ?></td>
                            <td><?= htmlspecialchars($appointment['last_name'] . ' ' . $appointment['name']) ?></td>
                            <td><?= htmlspecialchars($appointment['date']) ?></td>
                            <td><?= htmlspecialchars($appointment['time']) ?></td>
                            <td>
                                <?php if (empty($appointment['payment'])): ?>
                                    <?= '' ?>
                                <?php else: ?>
                                    <a href="generate_invoice.php?appointmentID=<?= urlencode($appointment['appointmentID']) ?>">Factură</a>
                                <?php endif; ?>
                            </td>
                            <td>
                                <form method="post" style="display:inline;">
                                    <input type="hidden" name="cancel_appointment" value="<?= $appointment['appointmentID'] ?>">
                                    <button type="button" onclick="confirmCancel(<?= $appointment['appointmentID'] ?>)">Anulează</button>
                                </form>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
        </div>
        <div class="form-container">
            <h2>Fă-ți o programare</h2>
            <form method="post" id="appointment-form">
                <div class="form-group">
                    <label for="type">Tip:</label>
                    <select id="type" name="type" required>
                        <option value="" disabled selected>Selectați tipul</option>
                        <?php foreach ($types as $type): ?>
                            <option value="<?= htmlspecialchars($type) ?>"><?= htmlspecialchars($type) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="dentistID">Medic stomatolog:</label>
                    <select id="dentistID" name="dentistID" required>
                        <option value="" disabled selected>Selectați dentistul</option>
                        <?php while ($dentist = $dentistsResult->fetch_assoc()): ?>
                            <option value="<?= $dentist['dentistID'] ?>"><?= htmlspecialchars($dentist['last_name'] . ' ' . $dentist['name']) ?></option>
                        <?php endwhile; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="date">Data:</label>
                    <input type="date" id="date" name="date" required>
                </div>
                <div class="form-group">
                    <label for="time">Ora:</label>
                    <input type="time" id="time" name="time" required>
                </div>
                <button type="submit">Fă-ți o programare</button>
            </form>
        </div>
    </div>
    <script>
        function confirmCancel(appointmentID) {
            Swal.fire({
                title: 'Doriți să anulați aceasta programare?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Da',
                cancelButtonText: 'Nu'
            }).then((result) => {
                if (result.isConfirmed) {
                    document.querySelector('input[name="cancel_appointment"][value="'+appointmentID+'"]').form.submit();
                }
            });
        }

        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('confirmLogout').style.display = 'block';
            document.getElementById('overlay').style.display = 'block';
        });

        function confirmLogout(choice) {
            if (choice) {
                window.location.href = 'login.php';
            } else {
                document.getElementById('confirmLogout').style.display = 'none';
                document.getElementById('overlay').style.display = 'none';
            }
        }

        document.addEventListener("DOMContentLoaded", function() {
            const urlParams = new URLSearchParams(window.location.search);
            if (urlParams.has('appointment_success')) {
                Swal.fire({
                    icon: 'success',
                    title: 'Programarea reușită!',
                    showConfirmButton: false,
                    timer: 2000
                }).then(() => {
                    window.location.href = 'clientappointments.php';
                });
            }

            if (urlParams.has('cancel_success')) {
                Swal.fire({
                    icon: 'success',
                    title: 'Anularea reușită!',
                    showConfirmButton: false,
                    timer: 2000
                }).then(() => {
                    window.location.href = 'clientappointments.php';
                });
            }
        });
    </script>

    <div id="overlay" class="overlay"></div>
    <div id="confirmLogout" class="confirm-logout">
        <p>Doriți să ieșiți din cont?</p>
        <button onclick="confirmLogout(true)">Da</button>
        <button onclick="confirmLogout(false)">Nu</button>
    </div>
</body>
</html>
