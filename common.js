

document.getElementById('logout').addEventListener('click', function(event) {
    event.preventDefault();
    document.getElementById('confirmLogout').style.display = 'block';
    document.getElementById('overlay').style.display = 'block';
});

function confirmLogout(choice) {
    if (choice) {
        window.location.href = 'login.php';
    } else {
        document.getElementById('confirmLogout').style.display = 'none';
        document.getElementById('overlay').style.display = 'none';
    }
}

function showSuccessMessage() {
    document.getElementById('successOverlay').style.display = 'block';
    document.getElementById('successMessage').style.display = 'block';
}

function closeSuccessMessage() {
    document.getElementById('successOverlay').style.display = 'none';
    document.getElementById('successMessage').style.display = 'none';
    window.location.href = 'clientprofil.php';  
}

let slideIndex = 0;
showSlides();

function showSlides() {
    let slides = document.getElementsByClassName("mySlides");
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) { slideIndex = 1 }
    slides[slideIndex - 1].style.display = "block";
    setTimeout(showSlides, 5000); 
}