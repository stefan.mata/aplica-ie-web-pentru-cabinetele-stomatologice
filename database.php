<?php 

$host="localhost";
$dbname="dentist_office";
$username="root";
$password="";

$mysqli=new mysqli($host,$username,$password,$dbname,3308);

if ($mysqli->connect_errno){
    die("Connection error: " . $mysqli->connect_errno);
}

return $mysqli;