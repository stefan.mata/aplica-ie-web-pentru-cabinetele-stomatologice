<?php
require 'vendor/autoload.php';

use Dompdf\Dompdf;

if (!isset($_GET['appointmentID'])) {
    die('Appointment ID is required');
}

$appointmentID = $_GET['appointmentID'];

$mysqli = require __DIR__ . "/database.php";

$sql = "SELECT appointments.type, appointments.date, appointments.time, appointments.payment, 
        dentists.last_name AS dentist_last_name, dentists.name AS dentist_name, 
        clients.last_name AS client_last_name, clients.name AS client_name
        FROM appointments
        JOIN dentists ON appointments.dentistID = dentists.dentistID
        JOIN clients ON appointments.clientID = clients.clientID
        WHERE appointments.appointmentID = ?";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param("i", $appointmentID);
$stmt->execute();
$result = $stmt->get_result();
$appointment = $result->fetch_assoc();

if (!$appointment) {
    die('Appointment not found');
}

$html = "
    <h1 style='text-align: center;'>Clinica Stomatologica</h1>
    <h2>Factura</h2>
    <p><strong>Nr. înregistrare:</strong> clinica123 | <strong>CIF:</strong> RO 123456789</p>
    <hr>
    <p><strong>Nume Client:</strong> {$appointment['client_last_name']} {$appointment['client_name']}</p>
    <p><strong>Doctor:</strong> {$appointment['dentist_last_name']} {$appointment['dentist_name']}</p>
    <hr>
    <h3>Detalii Programare:</h3>
    <table width='100%' border='1' cellspacing='0' cellpadding='5'>
        <thead>
            <tr>
                <th>Tip</th>
                <th>Data</th>
                <th>Ora</th>
                <th>Plata</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{$appointment['type']}</td>
                <td>{$appointment['date']}</td>
                <td>{$appointment['time']}</td>
                <td>{$appointment['payment']} Lei</td>
            </tr>
        </tbody>
    </table>
    <p><strong>Total:</strong> {$appointment['payment']} Lei</p>
    <p style='text-align: center;'>Multumim pentru ca ati ales clinica noastra!</p>
";

$dompdf = new Dompdf();
$dompdf->loadHtml($html);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();

$dompdf->stream("invoice_{$appointmentID}.pdf", array("Attachment" => 1)); 
?>
