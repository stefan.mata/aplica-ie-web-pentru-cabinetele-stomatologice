<?php
session_start();

if (!isset($_SESSION["user_id"])) {
    header("Location: login.php");
    exit;
}

$mysqli = require __DIR__ . "/database.php";

$user_id = $_SESSION["user_id"];
$sql = "SELECT users.username, users.email, clients.last_name, clients.name, clients.address, clients.phone 
        FROM users 
        JOIN clients ON users.userID = clients.userID 
        WHERE users.userID = ?";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param("i", $user_id);
$stmt->execute();
$result = $stmt->get_result();
$user = $result->fetch_assoc();

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $last_name = $_POST['last_name'];
    $name = $_POST['name'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];

    $sql = "UPDATE users SET username = ?, email = ? WHERE userID = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("ssi", $username, $email, $user_id);
    $stmt->execute();

    $sql = "UPDATE clients SET last_name = ?, name = ?, address = ?, phone = ? WHERE userID = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("ssssi", $last_name, $name, $address, $phone, $user_id);
    $stmt->execute();

    $_SESSION['update_success'] = true;

    header("Location: clientprofil.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profilul meu</title>
    <link rel="stylesheet" href="clienthomestyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
</head>
<body>
    <div class="navbar">
        <a href="clienthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="clientprofil.php">Profilul meu</a>
                <a href="clientappointments.php">Programările mele</a>
            </div>
        </div>
        <a href="#" class="nav-item" id="logout">Delogare</a>
    </div>
    <div class="content">
        <div class="form-container">
            <form method="post">
                <h2 class="form-title">Profilul meu</h2>
                <div class="form-group">
                    <label for="last_name">Nume:</label>
                    <input type="text" id="last_name" name="last_name" value="<?= htmlspecialchars($user['last_name']) ?>" required>
                </div>
                <div class="form-group">
                    <label for="name">Prenume:</label>
                    <input type="text" id="name" name="name" value="<?= htmlspecialchars($user['name']) ?>" required>
                </div>
                <div class="form-group">
                    <label for="username">Nume de utilizator:</label>
                    <input type="text" id="username" name="username" value="<?= htmlspecialchars($user['username']) ?>" required>
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" value="<?= htmlspecialchars($user['email']) ?>" required>
                </div>
                <div class="form-group">
                    <label for="address">Adresa:</label>
                    <input type="text" id="address" name="address" value="<?= htmlspecialchars($user['address']) ?>">
                </div>
                <div class="form-group">
                    <label for="phone">Telefon:</label>
                    <input type="text" id="phone" name="phone" value="<?= htmlspecialchars($user['phone']) ?>">
                </div>
                <button type="submit">Actualizează datele</button>
            </form>
        </div>
    </div>

    <?php if (isset($_SESSION['update_success']) && $_SESSION['update_success']): ?>
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Datele au fost actualizate!',
                showConfirmButton: false,
                timer: 2000
            });
        </script>
        <?php unset($_SESSION['update_success']); ?>
    <?php endif; ?>

    <script src="common.js"></script>

    <div id="overlay" class="overlay"></div>
    <div id="confirmLogout" class="confirm-logout">
        <p>Doriți să ieșiți din cont?</p>
        <button onclick="confirmLogout(true)">Da</button>
        <button onclick="confirmLogout(false)">Nu</button>
    </div>
</body>
</html>
