<?php
session_start();

if (!isset($_SESSION["user_id"]) || $_SESSION["role"] !== 'client') {
    header("Location: login.php");
    exit;
}

$mysqli = require __DIR__ . "/database.php";

$user_id = $_SESSION["user_id"];
$sqlClientID = "SELECT clientID FROM clients WHERE userID = ?";
$stmtClientID = $mysqli->prepare($sqlClientID);
$stmtClientID->bind_param("i", $user_id);
$stmtClientID->execute();
$resultClientID = $stmtClientID->get_result();
$client = $resultClientID->fetch_assoc();
$clientID = $client['clientID'];

$username = $_SESSION["username"];
$success = false; 

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['comment'])) {
    $comment = $_POST['comment'];

    $sqlInsert = "INSERT INTO feedback (clientID, comment) VALUES (?, ?)";
    $stmtInsert = $mysqli->prepare($sqlInsert);
    $stmtInsert->bind_param("is", $clientID, $comment);

    if ($stmtInsert->execute()) {
        $success = true;
    }
}
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Client Home</title>
    <link rel="stylesheet" href="clienthomestyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
    <div class="navbar">
        <a href="clienthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="clientprofil.php">Profilul meu</a>
                <a href="clientappointments.php">Programările mele</a>
            </div>
        </div>
        <a href="login.php" class="nav-item" id="logout">Delogare</a>
    </div>
    <div class="content">
        <h1>Bine ai venit, Client <span id="username"><?= htmlspecialchars($username) ?></span>!</h1>
        <div class="feedback-container">
            <p>Împărtășește-ne opinia ta! Feedback-ul tău ne ajută să îmbunătățim serviciile noastre.</p>
            <form method="post" class="feedback-form">
                <div class="form-group">
                    <textarea name="comment" required placeholder="Lasă un comentariu..."></textarea>
                </div>
                <button type="submit" class="submit-button">Adaugă comentariu</button>
            </form>
        </div>
    </div>

    <?php if ($success): ?>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            Swal.fire({
                icon: 'success',
                title: 'Feedback-ul a fost adăugat cu succes!',
                showConfirmButton: false,
                timer: 2000
            }).then(() => {
                window.location.href = 'clienthome.php';
            });
        });
    </script>
    <?php endif; ?>

    <script>
        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('confirmLogout').style.display = 'block';
            document.getElementById('overlay').style.display = 'block';
        });

        function confirmLogout(choice) {
            if (choice) {
                window.location.href = 'login.php';
            } else {
                document.getElementById('confirmLogout').style.display = 'none';
                document.getElementById('overlay').style.display = 'none';
            }
        }
    </script>

    <div id="overlay" class="overlay"></div>
    <div id="confirmLogout" class="confirm-logout">
        <p>Doriți să ieșiți din cont?</p>
        <button onclick="confirmLogout(true)">Da</button>
        <button onclick="confirmLogout(false)">Nu</button>
    </div>
</body>
</html>
