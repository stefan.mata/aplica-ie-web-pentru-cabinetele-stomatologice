<?php
session_start();

if (!isset($_SESSION["user_id"]) || $_SESSION["role"] !== 'dentist') {
    header("Location: login.php");
    exit;
}

$mysqli = require __DIR__ . "/database.php";


$currentMonth = isset($_GET['month']) ? (int)$_GET['month'] : date('m');
$currentYear = isset($_GET['year']) ? (int)$_GET['year'] : date('Y');
$selectedDate = isset($_GET['date']) ? $_GET['date'] : date('Y-m-d');

$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);

$sqlDentistID = "SELECT dentistID FROM dentists WHERE userID = ?";
$stmtDentistID = $mysqli->prepare($sqlDentistID);
$stmtDentistID->bind_param("i", $_SESSION["user_id"]);
$stmtDentistID->execute();
$resultDentistID = $stmtDentistID->get_result();
$dentist = $resultDentistID->fetch_assoc();
$dentistID = $dentist['dentistID'];

$sql = "SELECT appointments.appointmentID, clients.last_name AS client_last_name, clients.name AS client_name,
        appointments.type, appointments.time
        FROM appointments
        JOIN clients ON appointments.clientID = clients.clientID
        WHERE appointments.date = ? AND appointments.confirm = 'Yes' AND appointments.dentistID = ?";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param("si", $selectedDate, $dentistID);
$stmt->execute();
$result = $stmt->get_result();

if (isset($_POST['cancel_appointment'])) {
    $appointmentID = $_POST['cancel_appointment'];

    $sqlDelete = "DELETE FROM appointments WHERE appointmentID = ?";
    $stmtDelete = $mysqli->prepare($sqlDelete);
    $stmtDelete->bind_param("i", $appointmentID);
    $stmtDelete->execute();

    header("Location: dentistappointments.php?date=$selectedDate&cancel_success=1");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Programările</title>
    <link rel="stylesheet" href="receptionistappointmentsstyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
    <div class="navbar">
        <a href="dentisthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="dentistprofil.php">Profilul meu</a>
                <a href="dentistclients.php">Pacienții mei</a>
                <a href="dentistappointments.php">Programul meu</a>
            </div>
        </div>
        <a href="#" class="nav-item" id="logout">Delogare</a>
    </div>
    <div class="content">
        <h1>Programările din <?= htmlspecialchars($selectedDate) ?></h1>
        
        <div class="calendar-container">
            <div class="calendar-header">
                <a href="dentistappointments.php?month=<?= $currentMonth == 1 ? 12 : $currentMonth - 1 ?>&year=<?= $currentMonth == 1 ? $currentYear - 1 : $currentYear ?>">&laquo; Previous</a>
                <span><?= date('F Y', strtotime("$currentYear-$currentMonth-01")) ?></span>
                <a href="dentistappointments.php?month=<?= $currentMonth == 12 ? 1 : $currentMonth + 1 ?>&year=<?= $currentMonth == 12 ? $currentYear + 1 : $currentYear ?>">Next &raquo;</a>
            </div>
            <div class="calendar-grid">
                <?php for ($day = 1; $day <= $daysInMonth; $day++): ?>
                    <a href="dentistappointments.php?date=<?= "$currentYear-$currentMonth-$day" ?>" class="<?= $selectedDate == "$currentYear-$currentMonth-$day" ? 'selected' : '' ?>">
                        <?= $day ?>
                    </a>
                <?php endfor; ?>
            </div>
        </div>

        <div class="table-container">
            <table>
                <thead>
                    <tr>
                        <th>Pacient</th>
                        <th>Tip</th>
                        <th>Ora</th>
                        <th>Acțiuni</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($result->num_rows > 0): ?>
                        <?php while ($appointment = $result->fetch_assoc()): ?>
                            <tr>
                                <td><?= htmlspecialchars($appointment['client_last_name'] . ' ' . $appointment['client_name']) ?></td>
                                <td><?= htmlspecialchars($appointment['type']) ?></td>
                                <td><?= htmlspecialchars($appointment['time']) ?></td>
                                <td>
                                    <form method="post" style="display:inline;">
                                        <input type="hidden" name="cancel_appointment" value="<?= $appointment['appointmentID'] ?>">
                                        <button type="button" onclick="confirmCancel(<?= $appointment['appointmentID'] ?>)">Anulează</button>
                                    </form>
                                </td>
                            </tr>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="4" style="text-align: center;">Nu există programări pentru data selectată!</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function confirmCancel(appointmentID) {
            Swal.fire({
                title: 'Doriți să anulați aceasta programare?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Da',
                cancelButtonText: 'Nu'
            }).then((result) => {
                if (result.isConfirmed) {
                    document.querySelector('input[name="cancel_appointment"][value="'+appointmentID+'"]').form.submit();
                }
            });
        }

        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('confirmLogout').style.display = 'block';
            document.getElementById('overlay').style.display = 'block';
        });

        function confirmLogout(choice) {
            if (choice) {
                window.location.href = 'login.php';
            } else {
                document.getElementById('confirmLogout').style.display = 'none';
                document.getElementById('overlay').style.display = 'none';
            }
        }

        document.addEventListener("DOMContentLoaded", function() {
            const urlParams = new URLSearchParams(window.location.search);
            if (urlParams.has('cancel_success')) {
                Swal.fire({
                    icon: 'success',
                    title: 'Programarea a fost anulată!',
                    showConfirmButton: false,
                    timer: 2000
                }).then(() => {
                    window.location.href = 'dentistappointments.php?date=<?= $selectedDate ?>';
                });
            }
        });
    </script>

    <div id="overlay" class="overlay"></div>
    <div id="confirmLogout" class="confirm-logout">
        <p>Doriți să ieșiți din cont?</p>
        <button onclick="confirmLogout(true)">Da</button>
        <button onclick="confirmLogout(false)">Nu</button>
    </div>
</body>
</html>
