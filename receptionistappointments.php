<?php
session_start();

if (!isset($_SESSION["user_id"]) || $_SESSION["role"] !== 'receptionist') {
    header("Location: login.php");
    exit;
}

$mysqli = require __DIR__ . "/database.php";

$currentMonth = isset($_GET['month']) ? (int)$_GET['month'] : date('m');
$currentYear = isset($_GET['year']) ? (int)$_GET['year'] : date('Y');
$selectedDate = isset($_GET['date']) ? $_GET['date'] : date('Y-m-d');

$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);

$sql = "SELECT appointments.appointmentID, clients.last_name AS client_last_name, clients.name AS client_name,
        dentists.last_name AS dentist_last_name, dentists.name AS dentist_name, appointments.type, appointments.time, 
        (SELECT COUNT(*) FROM appointments AS a 
        WHERE a.date = appointments.date AND a.time = appointments.time AND a.dentistID = appointments.dentistID AND a.confirm = 'Yes') AS disponibilitate
        FROM appointments
        JOIN clients ON appointments.clientID = clients.clientID
        JOIN dentists ON appointments.dentistID = dentists.dentistID
        WHERE appointments.date = ? AND appointments.confirm = 'No'";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param("s", $selectedDate);
$stmt->execute();
$result = $stmt->get_result();

if (isset($_POST['reject_appointment'])) {
    $appointmentID = $_POST['reject_appointment'];

    $sqlDelete = "DELETE FROM appointments WHERE appointmentID = ?";
    $stmtDelete = $mysqli->prepare($sqlDelete);
    $stmtDelete->bind_param("i", $appointmentID);
    $stmtDelete->execute();

    header("Location: receptionistappointments.php?date=$selectedDate&cancel_success=1");
    exit;
}

if (isset($_POST['confirm'])) {
    $appointmentID = $_POST['appointmentID'];
    $payment = $_POST['payment'];

    $sqlUpdate = "UPDATE appointments SET confirm = 'Yes', payment = ? WHERE appointmentID = ?";
    $stmtUpdate = $mysqli->prepare($sqlUpdate);
    $stmtUpdate->bind_param("si", $payment, $appointmentID);
    $stmtUpdate->execute();

    header("Location: receptionistappointments.php?date=$selectedDate&confirm_success=1");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Programările</title>
    <link rel="stylesheet" href="receptionistappointmentsstyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
    <div class="navbar">
        <a href="receptionisthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="receptionistprofil.php">Profilul meu</a>
                <a href="administrareutilizatori.php">Administrare utilizatori</a>
                <a href="receptionistappointments.php">Administrare programări</a>
            </div>
        </div>
        <a href="#" class="nav-item" id="logout">Delogare</a>
    </div>
    <div class="content">
        <h1>Programările din <?= htmlspecialchars($selectedDate) ?></h1>
        
        <div class="calendar-container">
            <div class="calendar-header">
                <a href="receptionistappointments.php?month=<?= $currentMonth == 1 ? 12 : $currentMonth - 1 ?>&year=<?= $currentMonth == 1 ? $currentYear - 1 : $currentYear ?>">&laquo; Previous</a>
                <span><?= date('F Y', strtotime("$currentYear-$currentMonth-01")) ?></span>
                <a href="receptionistappointments.php?month=<?= $currentMonth == 12 ? 1 : $currentMonth + 1 ?>&year=<?= $currentMonth == 12 ? $currentYear + 1 : $currentYear ?>">Next &raquo;</a>
            </div>
            <div class="calendar-grid">
                <?php for ($day = 1; $day <= $daysInMonth; $day++): ?>
                    <a href="receptionistappointments.php?date=<?= "$currentYear-$currentMonth-$day" ?>" class="<?= $selectedDate == "$currentYear-$currentMonth-$day" ? 'selected' : '' ?>">
                        <?= $day ?>
                    </a>
                <?php endfor; ?>
            </div>
        </div>

        <div class="table-container">
            <table>
                <thead>
                    <tr>
                        <th>Client</th>
                        <th>Medic stomatolog</th>
                        <th>Ora</th>
                        <th>Disponibilitate</th>
                        <th>Acțiuni</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($result->num_rows > 0): ?>
                        <?php while ($appointment = $result->fetch_assoc()): ?>
                            <tr>
                                <td><?= htmlspecialchars($appointment['client_last_name'] . ' ' . $appointment['client_name']) ?></td>
                                <td><?= htmlspecialchars($appointment['dentist_last_name'] . ' ' . $appointment['dentist_name']) ?></td>
                                <td><?= htmlspecialchars($appointment['time']) ?></td>
                                <td><?= $appointment['disponibilitate'] > 0 ? 'Ocupat' : 'Disponibil' ?></td>
                                <td>
                                    <button onclick="confirmAppointment(<?= $appointment['appointmentID'] ?>, '<?= htmlspecialchars($appointment['client_last_name'] . ' ' . $appointment['client_name']) ?>', '<?= htmlspecialchars($appointment['type']) ?>')">Confirmă</button>
                                    <form method="post" style="display:inline;">
                                        <input type="hidden" name="reject_appointment" value="<?= $appointment['appointmentID'] ?>">
                                        <button type="button" onclick="confirmReject(<?= $appointment['appointmentID'] ?>)">Respinge</button>
                                    </form>
                                </td>
                            </tr>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5" style="text-align: center;">Nu există programări pentru data selectată!</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div id="confirmation-form-overlay" class="overlay"></div>
    <div id="confirmation-form" class="confirm-appointment-form">
        <form method="post">
            <input type="hidden" name="appointmentID" id="appointmentID">
            <p id="client-name"></p>
            <p id="appointment-type"></p>
            <div class="form-group">
                <label for="payment">Total:</label>
                <input type="text" id="payment" name="payment" required>lei
            </div>
            <button type="submit" name="confirm">Confirmare</button>
            <button type="button" onclick="closeConfirmationForm()">Anulează</button>
        </form>
    </div>

    <script>
        function confirmAppointment(appointmentID, clientFullName, appointmentType) {
            document.getElementById('appointmentID').value = appointmentID;
            document.getElementById('client-name').textContent = "Client: " + clientFullName;
            document.getElementById('appointment-type').textContent = "Tipul programării: " + appointmentType;
            document.getElementById('confirmation-form-overlay').style.display = 'block';
            document.getElementById('confirmation-form').style.display = 'block';
        }

        function closeConfirmationForm() {
            document.getElementById('confirmation-form-overlay').style.display = 'none';
            document.getElementById('confirmation-form').style.display = 'none';
        }

        function confirmReject(appointmentID) {
            Swal.fire({
                title: 'Doriți să respingeți aceasta programare?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Da',
                cancelButtonText: 'Nu'
            }).then((result) => {
                if (result.isConfirmed) {
                    document.querySelector('input[name="reject_appointment"][value="'+appointmentID+'"]').form.submit();
                }
            });
        }

        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('confirmLogout').style.display = 'block';
            document.getElementById('overlay').style.display = 'block';
        });

        function confirmLogout(choice) {
            if (choice) {
                window.location.href = 'login.php';
            } else {
                document.getElementById('confirmLogout').style.display = 'none';
                document.getElementById('overlay').style.display = 'none';
            }
        }

        document.addEventListener("DOMContentLoaded", function() {
            const urlParams = new URLSearchParams(window.location.search);
            if (urlParams.has('cancel_success')) {
                Swal.fire({
                    icon: 'success',
                    title: 'Programarea respinsă!',
                    showConfirmButton: false,
                    timer: 2000
                }).then(() => {
                    window.location.href = 'receptionistappointments.php?date=<?= $selectedDate ?>';
                });
            }
            if (urlParams.has('confirm_success')) {
                Swal.fire({
                    icon: 'success',
                    title: 'Programarea confirmată!',
                    showConfirmButton: false,
                    timer: 2000
                }).then(() => {
                    window.location.href = 'receptionistappointments.php?date=<?= $selectedDate ?>';
                });
            }
        });
    </script>

    <div id="overlay" class="overlay"></div>
    <div id="confirmLogout" class="confirm-logout">
        <p>Doriți să ieșiți din cont?</p>
        <button onclick="confirmLogout(true)">Da</button>
        <button onclick="confirmLogout(false)">Nu</button>
    </div>
</body>
</html>
