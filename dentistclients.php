<?php
session_start();

if (!isset($_SESSION["user_id"]) || $_SESSION["role"] !== 'dentist') {
    header("Location: login.php");
    exit;
}

$mysqli = require __DIR__ . "/database.php";

$user_id = $_SESSION["user_id"];
$sqlDentistID = "SELECT dentistID FROM dentists WHERE userID = ?";
$stmtDentistID = $mysqli->prepare($sqlDentistID);
$stmtDentistID->bind_param("i", $user_id);
$stmtDentistID->execute();
$resultDentistID = $stmtDentistID->get_result();
$dentist = $resultDentistID->fetch_assoc();
$dentistID = $dentist['dentistID'];

$sqlClients = "SELECT clients.clientID, clients.last_name, clients.name, users.email 
               FROM clients 
               JOIN users ON clients.userID = users.userID";
$resultClients = $mysqli->query($sqlClients);

$success = false;

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['clientID'])) {
    $clientID = $_POST['clientID'];
    $diagnostic = $_POST['diagnostic'];
    $treatment_plan = $_POST['treatment_plan'];

    $sqlInsert = "INSERT INTO medical (clientID, dentistID, diagnostic, treatment_plan) VALUES (?, ?, ?, ?)";
    $stmtInsert = $mysqli->prepare($sqlInsert);
    $stmtInsert->bind_param("iiss", $clientID, $dentistID, $diagnostic, $treatment_plan);

    if ($stmtInsert->execute()) {
        $success = true;
    } else {
        echo "<script>alert('A apărut o eroare. Încercați din nou.');</script>";
    }
}

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['add_treatment'])) {
    $medicalID = $_POST['medicalID'];
    $date = $_POST['date'];
    $type = $_POST['type'];
    $medication = $_POST['medication'];

    $sqlInsertTreatment = "INSERT INTO treatments (medicalID, date, type, medication) VALUES (?, ?, ?, ?)";
    $stmtInsertTreatment = $mysqli->prepare($sqlInsertTreatment);
    $stmtInsertTreatment->bind_param("isss", $medicalID, $date, $type, $medication);

    if ($stmtInsertTreatment->execute()) {
        $success = true;
    } else {
        echo "<script>alert('A apărut o eroare la adăugarea tratamentului. Încercați din nou.');</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista Pacienților</title>
    <link rel="stylesheet" href="dentistclientsstyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
    <div class="navbar">
        <a href="dentisthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="dentistprofil.php">Profilul meu</a>
                <a href="dentistclients.php">Pacienții mei</a>
                <a href="dentistappointments.php">Programul meu</a>
            </div>
        </div>
        <a href="#" class="nav-item" id="logout">Delogare</a>
    </div>
    <div class="content">
        <h1>Pacienții Mei</h1>
        <div class="table-container">
            <table>
                <thead>
                    <tr>
                        <th>Nume</th>
                        <th>Prenume</th>
                        <th>Email</th>
                        <th>Acțiuni</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($client = $resultClients->fetch_assoc()): ?>
                        <tr>
                            <td><?= htmlspecialchars($client['last_name']) ?></td>
                            <td><?= htmlspecialchars($client['name']) ?></td>
                            <td><?= htmlspecialchars($client['email']) ?></td>
                            <td>
                                <button onclick="addMedicalRecord(<?= $client['clientID'] ?>, '<?= htmlspecialchars($client['last_name'] . ' ' . $client['name']) ?>')">Adaugă fișa medicală</button>
                                <?php
                                $sqlMedical = "SELECT medicalID, diagnostic, treatment_plan FROM medical WHERE clientID = ?";
                                $stmtMedical = $mysqli->prepare($sqlMedical);
                                $stmtMedical->bind_param("i", $client['clientID']);
                                $stmtMedical->execute();
                                $resultMedical = $stmtMedical->get_result();

                                if ($resultMedical->num_rows > 0): ?>
                                    <button onclick="toggleMedicalRecordForm(<?= $client['clientID'] ?>)">Afișare fișe medicale</button>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php while ($medicalRecord = $resultMedical->fetch_assoc()): ?>
                            <tr class="medical-record-form client-<?= $client['clientID'] ?>" style="display: none;">
                                <td colspan="4">
                                    <h3>Fișa medicală pentru pacientul: <?= htmlspecialchars($client['last_name'] . ' ' . $client['name']) ?></h3>
                                    <p><strong>Afecțiune dentară:</strong> <?= htmlspecialchars($medicalRecord['diagnostic']) ?></p>
                                    <p><strong>Plan de tratament:</strong> <?= htmlspecialchars($medicalRecord['treatment_plan']) ?></p>

                                    <h4>Tratamente</h4>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Data</th>
                                                <th>Tip</th>
                                                <th>Medicație prescrisă</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sqlTreatments = "SELECT date, type, medication FROM treatments WHERE medicalID = ?";
                                            $stmtTreatments = $mysqli->prepare($sqlTreatments);
                                            $stmtTreatments->bind_param("i", $medicalRecord['medicalID']);
                                            $stmtTreatments->execute();
                                            $resultTreatments = $stmtTreatments->get_result();

                                            if ($resultTreatments->num_rows > 0) {
                                                while ($treatment = $resultTreatments->fetch_assoc()): ?>
                                                    <tr>
                                                        <td><?= htmlspecialchars($treatment['date']) ?></td>
                                                        <td><?= htmlspecialchars($treatment['type']) ?></td>
                                                        <td><?= htmlspecialchars($treatment['medication']) ?></td>
                                                    </tr>
                                                <?php endwhile;
                                            } else {
                                                echo '<tr><td colspan="3" style="text-align:center;">Nu există tratamente pentru acest client.</td></tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <button onclick="addTreatment(<?= $medicalRecord['medicalID'] ?>, '<?= htmlspecialchars($client['last_name'] . ' ' . $client['name']) ?>')">Adaugă tratament</button>
                                </td>
                            </tr>
                        <?php endwhile; ?>
                    <?php endwhile; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div id="medical-form-overlay" class="overlay"></div>
    <div id="medical-form" class="medical-form">
        <form method="post" id="medicalRecordForm">
            <input type="hidden" name="clientID" id="clientID">
            <p id="client-name"></p>
            <div class="form-group">
                <label for="diagnostic">Afecțiune dentară:</label>
                <select id="diagnostic" name="diagnostic" required>
                    <option value="" disabled selected>Selectați afecțiunea</option>
                    <?php
                    $sqlDiagnostic = "SHOW COLUMNS FROM medical LIKE 'diagnostic'";
                    $resultDiagnostic = $mysqli->query($sqlDiagnostic);
                    $rowDiagnostic = $resultDiagnostic->fetch_assoc();
                    preg_match("/^enum\(\'(.*)\'\)$/", $rowDiagnostic['Type'], $matches);
                    $enumValues = explode("','", $matches[1]);

                    foreach ($enumValues as $value) {
                        echo "<option value='" . htmlspecialchars($value) . "'>" . htmlspecialchars($value) . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="treatment_plan">Plan de tratament:</label>
                <input type="text" id="treatment_plan" name="treatment_plan" required>
            </div>
            <button type="submit">Adaugă</button>
            <button type="button" onclick="closeMedicalForm()">Anulează</button>
        </form>
    </div>

    <div id="treatment-form-overlay" class="overlay"></div>
    <div id="treatment-form" class="medical-form">
        <form method="post" id="treatmentForm">
            <input type="hidden" name="medicalID" id="treatmentMedicalID">
            <div class="form-group">
                <label for="date">Data:</label>
                <input type="date" id="date" name="date" required>
            </div>
            <div class="form-group">
                <label for="type">Tip de tratament:</label>
                <select id="type" name="type" required>
                    <option value="" disabled selected>Selectați tipul</option>
                    <?php
                    $sqlType = "SHOW COLUMNS FROM treatments LIKE 'type'";
                    $resultType = $mysqli->query($sqlType);
                    $rowType = $resultType->fetch_assoc();
                    preg_match("/^enum\(\'(.*)\'\)$/", $rowType['Type'], $matches);
                    $enumValuesType = explode("','", $matches[1]);

                    foreach ($enumValuesType as $value) {
                        echo "<option value='" . htmlspecialchars($value) . "'>" . htmlspecialchars($value) . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="medication">Medicație:</label>
                <input type="text" id="medication" name="medication" required>
            </div>
            <button type="submit" name="add_treatment">Adaugă tratament</button>
            <button type="button" onclick="closeTreatmentForm()">Anulează</button>
        </form>
    </div>

    <script>
        function addMedicalRecord(clientID, clientName) {
            document.getElementById('clientID').value = clientID;
            document.getElementById('client-name').textContent = "Pacient: " + clientName;
            document.getElementById('medical-form-overlay').style.display = 'block';
            document.getElementById('medical-form').style.display = 'block';
        }

        function closeMedicalForm() {
            document.getElementById('medical-form-overlay').style.display = 'none';
            document.getElementById('medical-form').style.display = 'none';
        }

        function toggleMedicalRecordForm(clientID) {
            const forms = document.querySelectorAll(`.medical-record-form.client-${clientID}`);
            forms.forEach(form => {
                form.style.display = form.style.display === 'none' ? 'table-row' : 'none';
            });
        }

        function addTreatment(medicalID, clientName) {
            document.getElementById('treatmentMedicalID').value = medicalID;
            document.getElementById('treatment-form-overlay').style.display = 'block';
            document.getElementById('treatment-form').style.display = 'block';
        }

        function closeTreatmentForm() {
            document.getElementById('treatment-form-overlay').style.display = 'none';
            document.getElementById('treatment-form').style.display = 'none';
        }

        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('confirmLogout').style.display = 'block';
            document.getElementById('overlay').style.display = 'block';
        });

        function confirmLogout(choice) {
            if (choice) {
                window.location.href = 'login.php';
            } else {
                document.getElementById('confirmLogout').style.display = 'none';
                document.getElementById('overlay').style.display = 'none';
            }
        }

        <?php if ($success): ?>
        document.addEventListener("DOMContentLoaded", function() {
            Swal.fire({
                icon: 'success',
                title: 'Adăugarea reușită!',
                showConfirmButton: false,
                timer: 2000
            }).then(() => {
                window.location.href = 'dentistclients.php';
            });
        });
        <?php endif; ?>
    </script>

    <div id="overlay" class="overlay"></div>
    <div id="confirmLogout" class="confirm-logout">
        <p>Doriți să ieșiți din cont?</p>
        <button onclick="confirmLogout(true)">Da</button>
        <button onclick="confirmLogout(false)">Nu</button>
    </div>
</body>
</html>
