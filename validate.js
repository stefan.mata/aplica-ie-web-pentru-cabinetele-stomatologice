document.getElementById('signup-form').addEventListener('submit', function(event) {
    event.preventDefault();

    var formIsValid = true;
    var formData = new FormData(this);

    var lastName = document.getElementById('last_name');
    var firstName = document.getElementById('name');
    var username = document.getElementById('username');
    var email = document.getElementById('email');
    var address = document.getElementById('adress');
    var phone = document.getElementById('phone');
    var password = document.getElementById('password');
    var passwordConfirmation = document.getElementById('password_confirmation');

    var elements = [lastName, firstName, username, email, address, phone, password, passwordConfirmation];

    elements.forEach(function(element) {
        var errorMessage = element.nextElementSibling;
        element.classList.remove('error');
        errorMessage.style.display = 'none';
        errorMessage.innerText = '';
    });

    if (lastName.value.trim() === '') {
        lastName.classList.add('error');
        lastName.nextElementSibling.innerText = 'Câmpul este gol';
        lastName.nextElementSibling.style.display = 'block';
        formIsValid = false;
    }

    if (firstName.value.trim() === '') {
        firstName.classList.add('error');
        firstName.nextElementSibling.innerText = 'Câmpul este gol';
        firstName.nextElementSibling.style.display = 'block';
        formIsValid = false;
    }

    if (username.value.length < 5) {
        username.classList.add('error');
        username.nextElementSibling.innerText = 'Numele de utilizator trebuie să conțină cel puțin 5 caractere';
        username.nextElementSibling.style.display = 'block';
        formIsValid = false;
    }

    if (address.value.trim() === '') {
        address.classList.add('error');
        address.nextElementSibling.innerText = 'Câmpul este gol';
        address.nextElementSibling.style.display = 'block';
        formIsValid = false;
    }

    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(email.value)) {
        email.classList.add('error');
        email.nextElementSibling.innerText = 'Introduceți un email valid';
        email.nextElementSibling.style.display = 'block';
        formIsValid = false;
    }

    if (phone.value.length < 7) {
        phone.classList.add('error');
        phone.nextElementSibling.innerText = 'Număr de telefon invalid';
        phone.nextElementSibling.style.display = 'block';
        formIsValid = false;
    }

    if (password.value !== passwordConfirmation.value) {
        passwordConfirmation.classList.add('error');
        passwordConfirmation.nextElementSibling.innerText = 'Parola nu se potrivește';
        passwordConfirmation.nextElementSibling.style.display = 'block';
        formIsValid = false;
    }

    if (!formIsValid) {
        return;
    }

    fetch('process-signup.php', {
        method: 'POST',
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        if (data.errors) {
            for (var key in data.errors) {
                var element = document.querySelector(`[name="${key}"]`);
                var errorMessage = element.nextElementSibling;
                element.classList.add('error');
                errorMessage.innerText = data.errors[key];
                errorMessage.style.display = 'block';
            }
        } else if (data.success) {
            var successMessage = document.getElementById('success-message');
            successMessage.style.display = 'block';
        }
    })
    .catch(error => console.error('Error:', error));
});

document.getElementById('ok-button').addEventListener('click', function() {
    window.location.href = 'login.php';
});
