<?php
session_start();

if (!isset($_SESSION["user_id"]) || $_SESSION["role"] !== 'dentist') {
    header("Location: login.php");
    exit;
}

$mysqli = require __DIR__ . "/database.php";

if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
} else {
    $user_id = $_SESSION["user_id"];
    $sql = "SELECT username FROM users WHERE userID = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("i", $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $user = $result->fetch_assoc();

    if ($user) {
        $username = $user['username'];
    } else {
        $username = "Utilizator";
    }
}

$sqlFeedback = "SELECT feedback.comment, clients.last_name, clients.name FROM feedback 
                JOIN clients ON feedback.clientID = clients.clientID";
$resultFeedback = $mysqli->query($sqlFeedback);
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Medic Stomatolog Home</title>
    <link rel="stylesheet" href="receptionisthomestyle.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
    <div class="navbar">
        <a href="dentisthome.php" class="nav-item">Acasă</a>
        <div class="dropdown">
            <button class="dropbtn">Meniu</button>
            <div class="dropdown-content">
                <a href="dentistprofil.php">Profilul meu</a>
                <a href="dentistclients.php">Pacienții mei</a>
                <a href="dentistappointments.php">Programul meu</a>
            </div>
        </div>
        <a href="logout.php" class="nav-item" id="logout">Delogare</a>
    </div>
    <div class="content">
        <h1>Bine ai venit, Medic Stomatolog <span id="username"><?= htmlspecialchars($username) ?></span>!</h1>
        
        <div class="feedback-display">
            <?php while ($feedback = $resultFeedback->fetch_assoc()): ?>
                <div class="feedback-container">
                    <p><strong><?= htmlspecialchars($feedback['last_name'] . ' ' . $feedback['name']) ?>:</strong></p>
                    <p>"<?= htmlspecialchars($feedback['comment']) ?>"</p>
                </div>
            <?php endwhile; ?>
        </div>
    </div>

    <script src="common.js"></script>

    <div id="overlay" class="overlay"></div>
    <div id="confirmLogout" class="confirm-logout">
        <p>Doriți să ieșiți din cont?</p>
        <button onclick="confirmLogout(true)">Da</button>
        <button onclick="confirmLogout(false)">Nu</button>
    </div>
</body>
</html>
